# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from django.conf.urls import *  # nopep8
from django.contrib.flatpages import views
from projectX.dashboard.views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    #url(r'^', include('projectX.dashboard.urls')),
    url(r'^home/$',
        HomeView.as_view(),
        name='home'),

    url(r'^dashboard/$',
        DashboardView.as_view(),
        name='dashboard'),

##FLATPAGES
    url(r'^about-us/$',
        views.flatpage, {'url': '/about-us/'},
        name='about-us'),

    url(r'^how-it-works/$',
        views.flatpage, {'url': '/how-it-works/'},
        name='how-it-works'),
    url(r'^faq/$',
        views.flatpage, {'url': '/faq/'},
        name='faq'),
    url(r'^contact-us/$',
        views.flatpage, {'url': '/contact-us/'},
        name='contact-us'),

    url(r'^accounts/', include('projectX.accounts.auth_urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
]
