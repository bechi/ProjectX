# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include


urlpatterns = [

    # Include base urls
    url(r'', include('projectX.urls.base')),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


