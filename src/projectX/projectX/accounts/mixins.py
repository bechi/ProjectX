# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.utils.decorators import classonlymethod

__all__ = ('UserAccessMixin', 'AnonymousAccessMixin')


class UserAccessMixin(object):
    """Authentication required mixin for class based views."""

    @classonlymethod
    def as_view(cls, **initkwargs):
        func = super(UserAccessMixin, cls).as_view(**initkwargs)

        @login_required
        def view_wrapper(request, *args, **kwargs):
            """Check authenticated user has staff abilities."""
            if request.user.is_staff or request.user.is_superuser:
                raise PermissionDenied
            return func(request, *args, **kwargs)

        return view_wrapper


class AnonymousAccessMixin(object):
    """Authentication required mixin for class based views."""

    @classonlymethod
    def as_view(cls, **initkwargs):
        func = super(AnonymousAccessMixin, cls).as_view(**initkwargs)

        def view_wrapper(request, *args, **kwargs):
            """Check authenticated user has staff abilities."""
            if not request.user.is_anonymous():
                raise PermissionDenied
            return func(request, *args, **kwargs)

        return view_wrapper
