# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from django.conf.urls import url
# from django.views.generic.base import TemplateView
from projectX.dashboard.views import *

urlpatterns = [

    url(r'^home/$',
        HomeView.as_view(),
        name='home'),

    url(r'^dashboard/$',
        DashboardView.as_view(),
        name='dashboard'),

]
