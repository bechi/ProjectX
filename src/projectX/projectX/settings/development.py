from projectX.settings.base import *  # nopep8

# debug
DEBUG = True

for backend in TEMPLATES:
    backend['OPTIONS']['debug'] = DEBUG

SITE_ID = 1
HTTPS = False

ALLOWED_HOSTS = (
    'localhost',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


SECRET_KEY = '!8#(y7qalq@=^dv0zq8f#*=o&b=*6f^fiqjd-if6=c$g8o*370'

# settings
INSTALLED_APPS += ('debug_toolbar',)

MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ROOT_URLCONF = 'projectX.urls.development'

def show_debug_toolbar(request):
    return True

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'projectX.settings.development.show_debug_toolbar',
}

INTERNAL_IPS = (
    '127.0.0.1',
    '10.0.2.15',
)
