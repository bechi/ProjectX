from escrow.settings.development import *  # nopep8

# Run tests in postgres running in ramdisk
# See http://blog.bessas.me/post/64464304829/running-your-django-tests-in-memory-with
#DEFAULT_TABLESPACE = 'ramfs'

SITE_ID = 4

HTTPS = False

# Use different directory for media files during testing.
MEDIA_ROOT = os.path.join(BASE_DIR, '../tests/media')

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

TEST_USERNAME = 'test@safekiwi.co.nz'
TEST_PASSWORD = 'safekiwi_password'

TEST_SUPERUSER_USERNAME = 'superuser@safekiwi.co.nz'

TEST_UNVERIFIED_COMPANY_OWNER_USERNAME = 'unverified.company.owner@safekiwi.co.nz'  # nopep8
TEST_VERIFIED_COMPANY_OWNER_USERNAME = 'verified.company.owner@safekiwi.co.nz'
TEST_VERIFIED_COMPANY_REPRESENTATIVE_USERNAME = 'verified.company.representative@safekiwi.co.nz'  # nopep8
TEST_VERIFIED_COMPANY_AUTHORIZER_USERNAME = 'verified.company.authorizer@safekiwi.co.nz'  # nopep8

TEST_UNVERIFIED_INDIVIDUAL_USERNAME = 'unverified.individual@safekiwi.co.nz'
TEST_BASIC_INDIVIDUAL_USERNAME = 'basic.individual@safekiwi.co.nz'
TEST_VERIFIED_INDIVIDUAL_USERNAME = 'verified.individual@safekiwi.co.nz'
