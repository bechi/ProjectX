# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import logging
from datetime import datetime

from django.apps import apps


__all__ = ('DatabaseHandler',)


class DatabaseHandler(logging.StreamHandler):

    """Logging handler to record log messages to the database."""

    def emit(self, record):
        """Record log messages to the database."""
        try:
            Log = apps.get_model('core', 'Log')

            l = Log.objects.create(
                message=self.format(record),
                by=getattr(record, 'by', None),
                is_public=getattr(record, 'is_public', False),
                level=record.levelname,
                stamp=datetime.fromtimestamp(record.created))

            if hasattr(record, 'context'):
                for context_obj in record.context:
                    l.add_context(context_obj)

            if hasattr(record, 'comments'):
                for comment in record.comments:
                    comment.add_context(l)

        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)
