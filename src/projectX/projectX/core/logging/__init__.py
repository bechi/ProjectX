# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import logging


def get_escrow_logger():
    """Return the escrow logging object."""
    return logging.getLogger('escrow')


logger = get_escrow_logger()


def _prepare_extra(by=None, is_public=False, comments=[], context=[]):
    """."""
    if type(context) not in (list, tuple):
        context = [context,]

    extra = {
        'by': by,
        'is_public': is_public,
        'comments': comments,
        'context': context,
    }

    return extra


def info(msg, by=None, is_public=False, comments=[], context=[]):
    """."""
    extra = _prepare_extra(by=by, is_public=is_public, context=context, 
                           comments=comments)
    logger.info(msg, extra=extra)


def warning(msg, by=None, is_public=False, comments=[], context=[]):
    """."""
    extra = _prepare_extra(by=by, is_public=is_public, context=context, 
                           comments=comments)
    logger.warning(msg, extra=extra)


def error(msg, by=None, is_public=False, comments=[], context=[]):
    """."""
    extra = _prepare_extra(by=by, is_public=is_public, context=context, 
                           comments=comments)
    logger.error(msg, extra=extra)


def critical(msg, by=None, is_public=False, comments=[], context=[]):
    """."""
    extra = _prepare_extra(by=by, is_public=is_public, context=context, 
                           comments=comments)
    logger.critical(msg, extra=extra)


def exception(msg, by=None, is_public=False, comments=[], context=[]):
    """."""
    extra = _prepare_extra(by=by, is_public=is_public, context=context, 
                           comments=comments)
    logger.exception(msg, extra=extra, exc_info=True)