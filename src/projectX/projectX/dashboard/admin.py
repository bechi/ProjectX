# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4: 

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function 

from django.contrib import admin
from django.contrib.flatpages.models import FlatPage

from escrow.content.forms.tiny_mce_flatpages import TinyMCEFlatpageForm


admin.site.unregister(FlatPage)


class TinyMCEFlatPageAdmin(admin.ModelAdmin):
    form = TinyMCEFlatpageForm
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites')}),
        (('Advanced options'), {'classes': ('collapse',),
                                'fields': (
                                    'enable_comments', 'registration_required',
                                    'template_name')}),
    )
    list_display = ('url', 'title')
    list_filter = ('sites', 'enable_comments', 'registration_required')
    search_fields = ('url', 'title')
    readonly_fields = ('url',)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


admin.site.register(FlatPage, TinyMCEFlatPageAdmin)
