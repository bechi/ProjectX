# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from django.views.generic import TemplateView
from projectX.accounts.mixins import UserAccessMixin
from projectX.accounts.mixins import AnonymousAccessMixin


__all__ = (
    'HomeView',
    'DashboardView',
)


class HomeView(AnonymousAccessMixin, TemplateView):
    """home page"""
    template_name = 'base.html'


class DashboardView(UserAccessMixin, TemplateView):
    """dashboard for logged users"""
    template_name = 'dashboard.html'
    MOST_RECENT_OBJECTS = 5
