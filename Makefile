.PHONY: install clean test coverage docs

# Default is to make development buildout. Set this as command line parameter
# to switch between buildouts. make install BUILDOUT=staging.cfg
BUILDOUT ?= development.cfg

# clean the build environment by removing all pyc files from the src directory.
clean:
	-find src -type f -name "*.pyc" -exec rm -f {} \;
	-find src -mindepth 1 -type d -name "*.egg-info" -exec rm -rf {} \;
	-rm -rf .coverage

install: clean
	mkdir -p downloads
	mkdir -p var/static
	mkdir -p var/media
	mkdir -p var/tmp
	mkdir -p var/run
	mkdir -p var/keys
	bin/python bootstrap.py
	bin/buildout -q -c $(BUILDOUT)
	bin/django collectstatic --noinput -v 0
	bin/django migrate --noinput

unit-test:
	bin/django test -t src/projectX/tests unit$(path) --settings=projectX.settings.test

test:
	bin/django test -t src/projectX/tests $(path) --settings=projectX.settings.test

clean-test:
	-find src/projectX/tests -name "2015" -exec rm -rf {} \;

# Coverage
coverage:
	coverage run --omit="*/management*,*/settings*,*/urls*,*/wsgi.py,*/migrations*,*/test*" \
		           --source projectX bin/django test -t src/projectX/tests \
							 --settings=projectX.settings.test --traceback
	coverage report -m

